#On commence par importer le ou les modules complémentaires pour réaliser le code.
import easygui as eg
#La 2ème étape est d'initialiser un compteur de fruit "count_fruits" qui permeetra de donner
#l'information à l'utilisateur sur le nombre de fruits qu'il a acheté
count_fruits = 0
#Après je crée une liste nommé choix qui va permettre de faire comprendre a EASYGUI les choix qui 
#qui seront soumis a l'utilisateur mais on definit aussi les messages et les titres pour l'interface
#graphique

choix = 'pomme', 'banane', 'orange', 'clementine', 'poire', 'pêche','fraise', 'kiwi'
question = 'Que voulez-vous dans votre sac de fruits?'
titre = 'Sac de fruit'
question2 = 'avez-vous fini votre commande?'
choix2 = 'oui', 'non'
#On réalise cette commande pour faire demarrer le compteur lorsque l'utilisateur choisit un fruits
rep = eg.buttonbox(question, titre, choices=choix)
count_fruits += 1
rep2 = eg.buttonbox(question2, titre, choices=choix2)

#La création de la boucle while permet de faire en sorte que tant que l'utilisateur dit que sa commande
#n'est pas terminée, on lui renvoie le choix de fruit
#Et à la fin on renvoie un message de bonne journée et on indique a l'utilisateur le nombre de fruit qu'il a 
#acheté
while rep2 == 'non':
    eg.buttonbox(question, titre, choices=choix)
    count_fruits += 1
    rep2 = eg.buttonbox(question2, titre, choices=choix2)
eg.msgbox(msg='nous vous souhaitons une bonne fin de journée')
print("Vous avez acheté", str(count_fruits), "fruits")